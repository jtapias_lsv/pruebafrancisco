from openpyxl.styles import Border, Alignment, Font, Side, PatternFill


def give_style(p_stylo):
    """
    method for give style to a cell

    :param p_stylo: an int that references a style
    :return: a dict with parameters: border, font, align, fill
    """

    my_stylo = {}
    if (p_stylo == '2'):
        ft = Font(size=16, bold=True, italic=True, color="4C4C63")
        bd = Border(left=Side(border_style="double"), right=Side(border_style="double"),
                    top=Side(border_style="double"), bottom=Side(border_style="double"))
        al = Alignment(horizontal='center', vertical='center')
        rl = PatternFill(fill_type="solid", fgColor="D2DAD1")

    else:
        ft = Font(size=12, bold=False, italic=False, color="000000")
        bd = Border(left=Side(border_style="none"), right=Side(border_style="none"),
                    top=Side(border_style="none"),
                    bottom=Side(border_style="none"))
        al = Alignment(horizontal='left', vertical='bottom')
        rl = PatternFill(fill_type="none")

    my_stylo.update({'fuente': ft})
    my_stylo.update({'borde': bd})
    my_stylo.update({'alineacion': al})
    my_stylo.update({'relleno': rl})

    return my_stylo


def load_excel(list,frs_line,w_sheet):
    """
    function that load xlsx file with information got in filter
    :param list: list of queryset objets
    :param frs_line: xlsx sheet row where start to input the information
    :param w_sheet: workbook sheet where info is stored
    :return: None
    """
    for l in list:
        w_sheet.cell(row=frs_line, column=2).value = str(l.get('customerId'))
        w_sheet.cell(row=frs_line, column=3).value = int(l.get('emailId'))
        w_sheet.cell(row=frs_line, column=4).value = str(l.get('parser'))
        w_sheet.cell(row=frs_line, column=5).value = str(l.get('option'))
        w_sheet.cell(row=frs_line, column=6).value = str(l.get('file'))
        w_sheet.cell(row=frs_line, column=7).value = str(l.get('created')[:10]+' '+l.get('created')[12:19])
        frs_line += 1

