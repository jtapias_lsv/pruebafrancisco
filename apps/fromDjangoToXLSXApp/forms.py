import datetime

from django import forms
from bootstrap_datepicker_plus import DatePickerInput

from  .models import FileParse, Customer
from .choices import ESTILO, TRUE_FALSE_CHOICES, FILTER

class FileParseForm(forms.Form):


    customer_id = forms.ModelChoiceField(queryset=Customer.objects.all(),
                                         widget=forms.Select(attrs={'class': 'form-control'}), required=True)

    sended = forms.ChoiceField(choices=TRUE_FALSE_CHOICES, label="Enviado",
                               initial='', widget=forms.Select(attrs={'class': 'form-control'}), required=True)

    filter = forms.ChoiceField(choices=FILTER, label="Filtro",
                               initial='', widget=forms.Select(attrs={'class': 'form-control'}), required=True)

    fecha_ini = forms.DateField(label='Fecha Inicial', widget=DatePickerInput(format='%Y-%m-%d'))
    fecha_fin = forms.DateField(label='Fecha Final', widget=DatePickerInput(format='%Y-%m-%d'))

    style = forms.ChoiceField(choices=ESTILO, label="Estilo del Reporte",
                               initial=1, widget=forms.Select(attrs={'class': 'form-control'}), required=True)



    def clean(self):
        cleaned_data = super(FileParseForm, self).clean()
        fecha_ini = cleaned_data.get('fecha_ini')
        fecha_fin = cleaned_data.get('fecha_fin')
        if fecha_fin <= datetime.date.today():
            if fecha_ini > fecha_fin:
                raise forms.ValidationError("Fecha inicial no puede ser mayor a la fecha final")
        else:
            raise forms.ValidationError("Fecha final no puede ser mayor a hoy")

        return cleaned_data