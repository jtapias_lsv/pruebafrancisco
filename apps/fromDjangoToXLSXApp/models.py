from django.db import models
from .choices import PRIMERA, PAR, OPT, BYMAIL


class Customer(models.Model):
    idCustomer = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    parser = models.CharField(max_length=1, choices=PAR)

    def __str__(self):
        return self.name

class FileParse(models.Model):
    idFileParser = models.AutoField(primary_key=True)
    customerId = models.ForeignKey(Customer, on_delete=models.CASCADE)
    emailId = models.IntegerField(null=True)
    emailDate = models.DateField()
    parser = models.CharField(max_length=1, choices=PAR, default=PRIMERA)
    option = models.CharField(max_length=20, choices=OPT, default=BYMAIL)
    sended = models.BooleanField(default=False)
    file = models.FileField()
    created = models.DateTimeField(auto_now_add=True,editable=False, null=True)

    def __str__(self):
        return str(self.customerId)+" - "+self.option


