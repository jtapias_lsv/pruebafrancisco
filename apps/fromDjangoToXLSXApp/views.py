from datetime import datetime

from django.shortcuts import render
from openpyxl import Workbook


from django.http import HttpResponse
from django.views.generic import TemplateView


from .models import FileParse
from .forms import FileParseForm
from .tasks import get_post_req
from .functions import give_style, load_excel

class Generar_xlsx(TemplateView):
    model = FileParse
    template_name = 'fromDjangoToXLSX/filtro.html'
    form_class = FileParseForm

    def get(self,request):
        return render(request, self.template_name,{'form': self.form_class})

    def form_invalid(self, form):
        return super(Generar_xlsx,self).form_invalid(form)




    def post(self,request):

        my_form = FileParseForm(request.POST)
        if my_form.is_valid():

            filtered = request.POST['filter']
            customer = request.POST['customer_id']
            date_ini = request.POST['fecha_ini']
            date_fin = request.POST['fecha_fin']
            sended = request.POST['sended']
            style = request.POST['style']

            info_list = get_post_req.delay(filtered, customer, date_ini, date_fin, sended).get()


            workbook = Workbook()
            worksheet = workbook.active
            worksheet.title = 'FileParse'

            worksheet['B1'] = 'REPORT'

            worksheet.merge_cells('B1:G1')

            titles = ['customer', 'emailId', 'parse', 'option', 'file_name', 'creation']


            first_line = 4
            load_excel(info_list, first_line, worksheet)

            line_title = 2

            my_style = give_style(style)

            for col_num, column_title in enumerate(titles, 2):
                cell = worksheet.cell(row=line_title, column=col_num)
                cell.value = column_title
                cell.font = my_style.get('fuente')
                cell.border = my_style.get('borde')
                cell.alignment = my_style.get('alineacion')
                cell.fill = my_style.get('relleno')




            response = HttpResponse(
                content_type = 'application/ms-excel'
            )
            response['Content-Disposition'] = 'attachment; filename = report_{date}.xlsx'.format(
                date=datetime.now().strftime("%d%m%Y-%H%M%S"),
            )


            workbook.save(response)

            return response

        return render(request, "fromDjangoToXLSX/filtro.html", {'form':my_form})








