from pruebaFrancisco.celery import app
from .models import FileParse


#app = get_current_app()


@app.task()
def get_post_req(filtered, customer, date_ini, date_fin, sended):
    """

    :param filtered: (str) option between email_date o creation_date
    :param customer: (int) customer id
    :param date_ini: (date) range init date
    :param date_fin: (date) range final date
    :param sended: (bool) option if fileparser was sended or not
    :return: (list) a list of dictionaries with parser objects information
    """

    if (filtered == 'ED'):
        lista = FileParse.objects.filter(customerId=customer,
                                           emailDate__gte=date_ini,
                                           emailDate__lte=date_fin,
                                           sended=sended
                                           ).values('customerId','emailId','parser','option','file','created')
    else:
        lista = FileParse.objects.filter(customerId=customer,
                                           created__gte=date_ini,
                                           created__lte=date_fin,
                                           sended=sended
                                           ).values('customerId','emailId','parser','option','file','created')



    return list(lista)