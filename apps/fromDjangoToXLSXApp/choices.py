PRIMERA = 'A'
SEGUNDA = 'B'
TERCERA = 'C'

BYMAIL = 'byEmail'
UPLOADBYUSER = 'uploadByUser'

PAR = (
    (PRIMERA, 'Primera'),
    (SEGUNDA, 'Segunda'),
    (TERCERA, 'Tercera')
)

OPT = (
    (BYMAIL, 'email'),
    (UPLOADBYUSER, 'user')
)

ESTILO = (
    (1, 'Basico'),
    (2, 'Formal')
)

TRUE_FALSE_CHOICES = (
    (True, 'Yes'),
    (False, 'No')
)

FILTER = (
    ('ED', 'Email_Date'),
    ('CD', 'Creation_Date')
)