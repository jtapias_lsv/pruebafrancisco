from django.contrib import admin

from .models import Customer, FileParse

admin.site.register(Customer)
admin.site.register(FileParse)
