from django.apps import AppConfig


class FromdjangotoxlsxappConfig(AppConfig):
    name = 'fromDjangoToXLSXApp'
