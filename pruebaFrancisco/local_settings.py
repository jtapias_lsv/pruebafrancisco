# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'pruebafrancisco',
        'USER': 'fcopruebauser',
        'PASSWORD': 'contra123',
        'HOST': 'localhost',
        'PORT': 5432,
        'CHARSET': 'UTF-8'
    }
}

AUTH_PASSWORD_VALIDATORS = []

INTERNAL_IPS = ['127.0.0.1','localhost']

CELERY_TASK_ALWAYS_EAGER =  False

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.lsv-tech.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'jtapias@lsv-tech.com'
EMAIL_HOST_PASSWORD = 'Simelase@1324'
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False